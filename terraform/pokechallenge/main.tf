# sending an API request to endpoint pikachu
data "http" "pika" {
  url = "https://pokeapi.co/api/v2/pokemon/pikachu/"

  # add the optional request header
  request_headers = {
    Accept = "application/json"
  }
}
