# output for data received from pika
output "pika_data" {
  description = "Shows response from poke API"
  value       = data.http.pika.response_body
}

# output for legal easy to read json
output "pika_data_read" {
  description = "Easier to read pika data"
  value       = jsondecode(data.http.pika.response_body)
}
