import requests

url = "https://numbersapi.p.rapidapi.com/6/21/date"

querystring = {"fragment":"true","json":"true"}

headers = {
	"X-RapidAPI-Key": "36e353aa6fmsh52a52e6903e6758p17ee32jsn21f839d7f64f",
	"X-RapidAPI-Host": "numbersapi.p.rapidapi.com"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
